

<?php
require 'animal.php';
require 'Frog.php';
require 'Ape.php';

$sheep = new Animal("shaun");
$animal2 = new Frog("buduk", "no", 4);
$animal3 = new Ape("kera sakti", "no", 2);

echo "Name : " . $sheep->getName() . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cool Blooded : " . $sheep->cold_blooded . "<br>" . "<br>"; // "no"

echo "Name : " . $animal2->getName() . "<br>";
echo "Legs : " . $animal2->legs . "<br>";
echo "Cool Blooded : " . $animal2->cold_blooded . "<br>";
echo "Yell : " . $animal2->jump() . "<br>" . "<br>";

echo "Name : " . $animal3->getName() . "<br>";
echo "Legs : " . $animal3->legs . "<br>";
echo "Cool Blooded : " . $animal3->cold_blooded . "<br>";
echo "Yell : " . $animal3->yell() . "<br>";

?>