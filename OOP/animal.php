<?php
class Animal
{
    public $legs = 4;
    public $cold_blooded = "no";

    function __construct($name, $cold_blooded = "no", $legs = 2)
    {
        $this->name = $name;
    }

    function getName()
    {
        return $this->name;
    }
}
